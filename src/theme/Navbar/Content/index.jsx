import React, { useEffect, useState } from "react";
import Content from "@theme-original/Navbar/Content";
import { supabase } from "../../../supabase";

export default function ContentWrapper(props) {
  const [user, setUser] = useState(null);

  useEffect(() => {
    const session = supabase.auth.session();
    setUser(session?.user ?? null);
    const { data: authListener } = supabase.auth.onAuthStateChange(
      async (event, session) => {
        const currentUser = session?.user;
        setUser(currentUser ?? null);
        console.log(currentUser, 'currentUser')
      }
    );

    return () => {
      authListener?.unsubscribe();
    };
  }, [user]);

  const onLogOut = () => {
    supabase.auth.signOut();
    document.body.style.setProperty("--is-auth", "initial");
  };
  return (
    <>
      <div style={{ maxWidth: '1430px' }} className="items-center justify-between flex w-full mx-auto">
        <Content {...props} />
        <div className="flex !justify-end space-x-4">
          <div class="buttonscont">
            <a href="" class="langbtn !no-underline	">
              Get in touch

            </a>
          </div>
          {user &&

            <div className="flex cursor-pointer  group relative ml-auto !w-fit items-center">
              <span>{user.email.split('@')[0] || ''}</span>
              <div className="absolute group-hover:flex flex-col hidden top-full w-32 bg-white right-0 shadow-md rounded-sm pt-2">
                <div className="p-2 truncate border-b">
                  {user.email || ''}
                </div>
                <button
                  className="py-3"
                  onClick={onLogOut}
                  style={{ height: "100%", textAlign: "center" }}
                >
                  Logout
                </button>
              </div>
              <svg className="w-8 h-8" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-6-3a2 2 0 11-4 0 2 2 0 014 0zm-2 4a5 5 0 00-4.546 2.916A5.986 5.986 0 0010 16a5.986 5.986 0 004.546-2.084A5 5 0 0010 11z" clipRule="evenodd" /></svg>
            </div>
          }
        </div>
      </div>


    </>
  );
}
