import { useHistory } from "@docusaurus/router";
import Layout from "@theme/Layout";
import React, { useEffect } from "react";
import AuthForm from "../../components/AuthForm/AuthForm";
import { supabase } from "../../supabase";

const Auth = () => {
  const history = useHistory();
  useEffect(() => {
    const user = supabase.auth?.session()?.user;
    const checkIfFirstTime = async () => {
      return await supabase
        .from("profiles")
        .select("id,  avatarurl")
        .filter("id", "eq", user.id);
    };
    if (user) {
      document.body.style.setProperty("--is-auth", "none");
      history.push("/");
    }
  }, []);
  return (
    <Layout
      title={`Auth`}
      description="Description will go into a meta tag in <head />"
    >
      <AuthForm />
    </Layout>
  );
};

export default Auth;
